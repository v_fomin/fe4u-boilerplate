const { isValidPhoneNumber, parse } = require('libphonenumber-js');
//const { randomUserMock, additionalUsers } = require('./FE4U-Lab2-mock');
const testModules = require('./test-module');
require('../css/app.css');
require('./FE4U-Lab2-mock');
const L = require('leaflet');
const _ = require('lodash');

const COURSES = [
    "Mathematics", "Physics", "English", "Computer Science", "Dancing", 
    "Chess", "Biology", "Chemistry", "Law", "Art", "Medicine", "Statistics"
];
const ADDITIONAL_PROPS = [
    {
        propName: "favorite",
        defaultValue: "false"
    },
    {
        propName: "course",
        defaultValue: "getCourse()"
    },
    {
        propName: "bg_color",
        defaultValue: "'#ffffff'"
    },
    {
        propName: "note",
        defaultValue: "'Additional notes'"
    }
];
const REGIONS_OF_COUNTRIES = {
    "europe": ["Germany", "Ukraine", "France", "Ireland", "Finland", "Switzerland", "Spain", "Norway", "Denmark", "Netherlands"],
    "north america": ["United States", "Canada"],
    "south america": ["Brazil"],
    "africa": ["Egypt"],
    "asia": ["China", "Japan", "Iran", "Israel", "Turkey"],
    "oceania": ["Australia", "New Zealand"],
};
const COUNTRY_TO_SHORT = {
    "Germany": "DE",
    "United States": "US",
    "Australia": "AU",
    "Finland": "FI",
    "Turkey": "TR",
    "New Zealand": "NZ",
    "Norway": "NO",
    "Ireland": "IE",
    "Canada": "CA",
    "France": "FR",
    "Switzerland": "CH",
    "Spain": "ES",
    "Denmark": "DK",
    "Iran": "IR",
    "Netherlands": "NL",
    "Ukraine": "UA"
};
const EMAIL_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
let teacherGenId = 0;

const TEACHER_LIST = document.querySelector(".teacher-list");
const FAV_RIGHT_ARROW = document.querySelector(".favorite-right");
const FILTER_CONTROLS = document.querySelectorAll(".filter-control input, .filter-control select");
const TEACHER_TABLE = document.querySelector(".table-body");
const TEACHER_TABLE_HEAD = document.querySelectorAll(".table-head tr th");
const SEARCH_FORM = document.querySelector(".input-text").parentElement;
const TEACHER_INFO_POPUP = {
    popup: document.querySelector("#info_teacher_window"),
    image: document.querySelector("#teacher_image_full"),
    name: document.querySelector("#teacher_name_full"),
    course: document.querySelector("#teacher_speciality_full"),
    region: document.querySelector("#teacher_region_full"),
    ageGender: document.querySelector("#teacher_age_gender_full"),
    email: document.querySelector("#teacher_email_full address a"),
    phone: document.querySelector("#teacher_phone_full address"),
    favorite: document.querySelector("#teacher_favorite_full"),
    note: document.querySelector("#teacher_description_full"),
    photo: document.querySelector("#teacher_image_full"),
    untilBday: document.querySelector("#teacher_until_bday")
}
const TEACHER_FORM = {
    form: document.querySelector("#add_teacher_window").children[1].children[0],
    name: document.querySelector("#add_teacher_name"),
    course: document.querySelector("#add_teacher_spec"),
    country: document.querySelector("#add_teacher_country"),
    city: document.querySelector("#add_teacher_city"),
    email: document.querySelector("#add_teacher_email"),
    phone: document.querySelector("#add_teacher_phone"),
    b_date: document.querySelector("#add_teacher_birthday"),
    genderMale: document.querySelector("#add_teacher_male"),
    genderFemale: document.querySelector("#add_teacher_female"),
    color: document.querySelector("#add_teacher_color"),
    note: document.querySelector("#add_teacher_notes"),
}

const TABLE_TEACHER_NUM = 10;

let tablePageCounter = 1;

// let baseTeachers = displayTeachers(randomUserMock, additionalUsers);
let baseTeachers, currentTeachers;

let map, marker;
const MAP = document.querySelector("#map");

loadTeachers(50);

let chart;
const CHART = document.getElementById('myChart');

// Task 5.1
// Add marker
function initMap() {
    map = L.map('map');
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
    document.querySelector("#teacher_map_full").addEventListener("click", (event) => {
        event.preventDefault();
        MAP.style.opacity = `${+!+MAP.style.opacity}`;
    });
}

// Task 5.2
function drawChart() {
    const courses = Array.from(new Set(_.map(currentTeachers, teacher => teacher.course)));
    const numberOfTeachersForCourse = _.map(courses, course => _.filter(currentTeachers, teacher => teacher.course == course).length);
    if (chart) {
        chart.destroy();
    }
    chart = new Chart(CHART, {
        type: 'pie',
        data: {
            labels: courses,
            datasets: [{
                label: 'Number of Teachers',
                data: numberOfTeachersForCourse,
                borderWidth: 1
            }]
        },
    });
}

// Task 5.4
function untilBday(birthday) {
    let bDay = dayjs(birthday);
    let now = dayjs();
    bDay = bDay.year(now.year());
    const result = bDay.diff(now, "day");
    if (result <= 0) {
        bDay = bDay.year(now.year() + 1);
        return bDay.diff(now, "day");
    }
    return result;
}

// Task 4.1
async function loadTeachers(numberOfTeachers) {
    const teachers = (await (await fetch(`https://randomuser.me/api?results=${numberOfTeachers}`)).json()).results;
    let additionalTeachers = [];
    try {
        additionalTeachers = await (await fetch(`http://localhost:3000/teachers`)).json();
    } catch (error) {
        console.log("Server with additional teachers is off");
    }
    currentTeachers = baseTeachers = displayTeachers(teachers, additionalTeachers);
    attachFavHandlers();
    attachTchrHandlers();
    prepareFilters();
    //prepareSortingTable();
    prepareSearchField();
    prepareAddTeacherForm();
    //addTableNavButtons();
    //attachPaginationHandlers();
    dayjs().format();
    initMap();
    drawChart();
}

// Lab 4 helper functions
function showTableTeachers(startTeacher = 1) {
    const tableTeachers = document.querySelectorAll("tr");
    for(let i = 1; i < tableTeachers.length; i++) {
        if (i >= startTeacher && i < startTeacher + TABLE_TEACHER_NUM) {
            tableTeachers[i].style.display = 'table-row';
        } else {
            tableTeachers[i].style.display = 'none';
        }
    }
}

// Task 4.3
function addTableNavButtons() {
    const tableTeachers = document.querySelectorAll("tr");
    if (tableTeachers.length > TABLE_TEACHER_NUM) {
        document.querySelectorAll(".separator-navigation.hidden")[1].classList.remove("hidden");
        document.querySelectorAll(".button-navigation.hidden")[1].classList.remove("hidden");
    }
    for (let i = TABLE_TEACHER_NUM + 1; i < tableTeachers.length; i++) {
        if ((i-1) % TABLE_TEACHER_NUM === 0) {
            const navButtons = document.querySelectorAll(".button-navigation");
            navButtons[navButtons.length - 2].insertAdjacentHTML(
                "afterend",
                `<button class="button-navigation" type="button">${++tablePageCounter}</button>`)
        }
    }
}

// Task 4.3
function attachPaginationHandlers() {
    const navButtons = document.querySelectorAll(".button-navigation");
    for (const navButton of navButtons) {
        navButton.addEventListener("click", (event) => {
            if (event.target.classList.contains("inactive")) {
                return;
            }
            _.map([...navButtons], navBut => navBut.classList.remove('inactive'));
            if (event.target.textContent === "First") {
                event.target.classList.add("hidden");
                navButtons[1].classList.add("inactive");
            } else if (event.target.textContent === "Last") {
                event.target.classList.add("hidden");
                navButtons[navButtons.length - 2].classList.add("inactive");
            } else {
                event.target.classList.add("inactive");
            }
            if (+event.target.textContent === 1 || event.target.textContent === "First") {
                navButtons[0].classList.add("hidden");
                document.querySelector(".separator-navigation").classList.add("hidden");
            } else if (navButtons[0].classList.contains("hidden")){
                navButtons[0].classList.remove("hidden");
                document.querySelector(".separator-navigation").classList.remove("hidden");
            }
            if (+event.target.textContent === tablePageCounter || event.target.textContent === "Last") {
                navButtons[navButtons.length - 1].classList.add("hidden");
                document.querySelectorAll(".separator-navigation")[1].classList.add("hidden");
            } else if (navButtons[navButtons.length - 1].classList.contains("hidden")){
                navButtons[navButtons.length - 1].classList.remove("hidden");
                document.querySelectorAll(".separator-navigation")[1].classList.remove("hidden");
            }

            if (event.target.textContent === "First") {
                showTableTeachers();
            } else if (event.target.textContent === "Last") {
                showTableTeachers((tablePageCounter - 1) * TABLE_TEACHER_NUM + 1);
            } else {
                showTableTeachers((event.target.textContent - 1) * TABLE_TEACHER_NUM + 1);
            }
        });
    }
}

// Tasks 3.1 and 4.2
function displayTeachers(teachers, additionalTeachers) {
    let parsedTeachers = parseTeachers(teachers);
    if (additionalTeachers) {
        parsedTeachers = addNewTeachers(parsedTeachers, additionalTeachers);
    }
    let teacherList = '';
    let favTeacherList = '';
    //let teacherTable = '';
    _.forEach(parsedTeachers, parsedTeacher => {
        teacherList += getTeacherHTML(parsedTeacher);
        if (parsedTeacher.favorite) {
            favTeacherList += getFavTeacherHTML(parsedTeacher);
        }
        //teacherTable += getTableTeacherHTML(parsedTeacher);
    });
    addTeachersOnPage(teacherList);
    addFavoriteTeachersOnPage(favTeacherList);
    //addTeachersToTable(teacherTable);
    return parsedTeachers;
}

function onFavoriteClick(event) {
    const teacherItem = event.target.parentElement.parentElement;
    const tId = teacherItem.dataset.id;
    if(teacherItem.className.includes("favorite")) {
        teacherItem.classList.remove("favorite");
        document.querySelectorAll(`[data-id="${tId}"`)[1].remove();
        baseTeachers[baseTeachers.findIndex(tchr => tchr.id == tId)].favorite = false;
    } else {
        teacherItem.classList.add("favorite");
        baseTeachers[baseTeachers.findIndex(tchr => tchr.id == tId)].favorite = true;
        addFavoriteTeachersOnPage(
            getFavTeacherHTML({
                id: tId, 
                full_name: teacherItem.children[2].firstElementChild.textContent, 
                course: teacherItem.children[3].firstElementChild.textContent, 
                country: teacherItem.children[4].firstElementChild.textContent,
                picture_thumbnail: teacherItem.children[0].firstElementChild.src
            })
        );
        attachFavHandlers();
        attachTchrHandlers();
    }
}

function attachTchrHandlers() {
    const tchrItems = document.querySelectorAll(".teacher-image");
    _.forEach(tchrItems, tchrItem => tchrItem.addEventListener("click", showTeacherInfoPopup));
}

function attachFavHandlers() {
    const favIcons = document.querySelectorAll(".teacher-fav, .teacher-not-fav");
    _.forEach(favIcons, favIcon => favIcon.addEventListener("click", onFavoriteClick));
}

function showTeacherInfoPopup(event) {
    TEACHER_INFO_POPUP.popup.style.display = "block";
    TEACHER_INFO_POPUP.popup.parentElement.style.display = "block";
    let teacherId = event.target.parentElement.dataset.id ? event.target.parentElement.dataset.id : event.target.parentElement.parentElement.dataset.id;
    const teacher = baseTeachers[baseTeachers.findIndex(tchr => tchr.id == teacherId)];
    TEACHER_INFO_POPUP.image.alt = `${teacher.full_name.charAt(0)}.${teacher.full_name.split(' ')[1].charAt(0)}`;
    TEACHER_INFO_POPUP.name.textContent = teacher.full_name;
    TEACHER_INFO_POPUP.course.textContent = teacher.course;
    TEACHER_INFO_POPUP.region.textContent = `${teacher.city}, ${teacher.country}`;
    TEACHER_INFO_POPUP.ageGender.textContent = `${teacher.age}, ${teacher.gender}`;
    TEACHER_INFO_POPUP.email.textContent = teacher.email;
    TEACHER_INFO_POPUP.email.href = `mailto:${teacher.email}`;
    TEACHER_INFO_POPUP.phone.textContent = teacher.phone;
    TEACHER_INFO_POPUP.favorite.src = teacher.favorite ? "images/fav.png" : "images/not_fav.png";
    TEACHER_INFO_POPUP.note.textContent = teacher.note;
    TEACHER_INFO_POPUP.photo.src = teacher.picture_large;
    map.setView([+teacher.coordinates.latitude, +teacher.coordinates.longitude], 13);
    if(marker) {
        map.removeLayer(marker);
    }
    marker = L.marker([+teacher.coordinates.latitude, +teacher.coordinates.longitude]).addTo(map);
    TEACHER_INFO_POPUP.untilBday.textContent = untilBday(teacher.b_date);
}

// Tasks 3.2 and 4.2
function prepareFilters() {
    _.forEach(FILTER_CONTROLS, filterControl => filterControl.addEventListener("change", filterTeachers));
}

function filterTeachers(event) {
    const ages = FILTER_CONTROLS[0].value ? FILTER_CONTROLS[0].value.split(' ') : [0, 0];
    const region = FILTER_CONTROLS[1].value;
    const gender = FILTER_CONTROLS[2].value;
    // Add pics
    const photoOnly = FILTER_CONTROLS[3].checked;
    const favOnly = FILTER_CONTROLS[4].checked;
    currentTeachers = getFilteredTeachers(baseTeachers, region, ages[0], ages[1], gender, favOnly, photoOnly);
    TEACHER_LIST.innerHTML = '';
    //TEACHER_TABLE.innerHTML = '';
    let teacherList = '';
    //let tableTeacherList = '';
    _.forEach(currentTeachers, filteredTeacher => {
        teacherList += getTeacherHTML(filteredTeacher);
        //tableTeacherList += getTableTeacherHTML(filteredTeacher);
    });
    addTeachersOnPage(teacherList);
    //addTeachersToTable(tableTeacherList);
    searchTeachers();
}

// Tasks 3.3 and 4.2
function sortTeachers(event) {
    const headerClicked = event.target.nodeName === "DIV"
    let key = headerClicked ? event.target.textContent.toLowerCase() : event.target.parentElement.children[0].textContent.toLowerCase();
    switch(key) {
        case "name":
            key = "full_name";
            break;
        case "speciality":
            key = "course";
            break;
        case "age":
            key = "age";
            break;
        case "gender":
            key = "gender";
            break;
        case "nationality":
            key = "country";
            break;
    }
    const arrowElem = headerClicked ? event.target.parentElement.children[1] : event.target;
    const desc = arrowElem.textContent === '↑';
    _.forEach(document.querySelectorAll('.sort-arrow'), arrow => arrow.textContent = '↓');
    arrowElem.textContent = desc ? '↓' : '↑';
    TEACHER_TABLE.innerHTML = "";
    const sortedTeachers = getSortedTeachers(baseTeachers, key, desc);
    let teacherTableList = '';
    _.forEach(sortedTeachers, sortedTeacher => teacherTableList += getTableTeacherHTML(sortedTeacher));
    addTeachersToTable(teacherTableList);
}

function prepareSortingTable() {
    for(const tableHeader of TEACHER_TABLE_HEAD) {
        tableHeader.addEventListener('click', sortTeachers);
    }
}

// Tasks 3.4 and 4.2
function searchTeachers(event) {
    if (event) {
        event.preventDefault();
    }
    const query = SEARCH_FORM.children[0].value;
    const queryTeachers = getQueryTeachers(currentTeachers, query);
    TEACHER_LIST.innerHTML = '';
    //TEACHER_TABLE.innerHTML = '';
    let teacherList = '';
    //let tableTeacherList = '';
    _.forEach(queryTeachers, queryTeacher => {
        teacherList += getTeacherHTML(queryTeacher);
        //tableTeacherList += getTableTeacherHTML(queryTeacher);
    });
    addTeachersOnPage(teacherList);
    //addTeachersToTable(tableTeacherList);
    attachTchrHandlers();
    attachFavHandlers();
    drawChart();
}

function prepareSearchField() {
    SEARCH_FORM.addEventListener('submit', searchTeachers);
}

// Tasks 3.5 and 4.2, 4.4
function addTeacher(event) {
    event.preventDefault();
    const teacher = {
        id: ++teacherGenId,
        full_name: TEACHER_FORM.name.value,
        course: TEACHER_FORM.course.value,
        country: TEACHER_FORM.country.value,
        city: TEACHER_FORM.city.value,
        b_date: TEACHER_FORM.b_date.valueAsDate,
        age: calcAge(TEACHER_FORM.b_date.valueAsDate),
        phone: TEACHER_FORM.phone.value,
        email: TEACHER_FORM.email.value,
        state: TEACHER_FORM.city.value,
        favorite: false,
        bg_color: TEACHER_FORM.color.value,
        gender: TEACHER_FORM.genderMale.checked ? 'male' : 'female'
    };
    baseTeachers = addNewTeachers(baseTeachers, [teacher]);
    filterTeachers();
    //addTeachersToTable(getTableTeacherHTML(teacher));
    fetch('http://localhost:3000/teachers', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(teacher)
    })
        .then(response => response.json())
        .then(response => {
            console.log(JSON.stringify(response));
            closeAddTeacherWindow();
        });
}

function prepareAddTeacherForm() {
    TEACHER_FORM.form.addEventListener('submit', addTeacher);
}

// Helper functions Lab 3
function getTeacherHTML({id, favorite, full_name, course, country, picture_thumbnail}) {
    const teacherNames = full_name.split(" ");
    return `<div class="teacher-item${favorite ? " favorite" : ""}" data-id="${id}">
        <div class="teacher-image">
            <img alt="${teacherNames[0].charAt(0)}.${teacherNames[1].charAt(0)}" class="teacher-photo"${picture_thumbnail ?  ' src="'+picture_thumbnail+'"' : ""}/>
        </div>
        <div>
            <img alt="Fav" class="teacher-fav" src="images/fav.png"/>
            <img alt="Not fav" class="teacher-not-fav" src="images/not_fav.png"/>
        </div>
        <div class="teacher-name"><p>${teacherNames[0]} <br>${teacherNames[1]}</p></div>
        <div class="teacher-speciality"><p>${course}</p></div>
        <div class="teacher-nationality"><p>${country}</p></div>
        </div>`;
}

function getTableTeacherHTML({id, full_name, course, age, gender, country}) {
    return `<tr>
        <td><div>${full_name}</div></td>
        <td><div>${course}</div></td>
        <td><div>${age}</div></td>
        <td><div>${gender}</div></td>
        <td><div>${country}</div></td>
    </tr>`;
}

function getFavTeacherHTML({id, full_name, course, country, picture_thumbnail}) {
    const teacherNames = full_name.split(" ");
    return `<div class="teacher-item" data-id="${id ? id : ++teacherGenId}">
        <div class="teacher-image">
            <img alt="${teacherNames[0].charAt(0)}.${teacherNames[1].charAt(0)}" class="teacher-photo"${picture_thumbnail ?  ' src="'+picture_thumbnail+'"' : ""}/>
        </div>
        <div class="teacher-name"><p>${teacherNames[0]} <br>${teacherNames[1]}</p></div>
        <div class="teacher-speciality"><p>${course}</p></div>
        <div class="teacher-nationality"><p>${country}</p></div>
        </div>`;
}

function addTeachersToTable(teachers) {
    TEACHER_TABLE.insertAdjacentHTML(
        "beforeend",
        teachers
        );
    showTableTeachers();
}

function addTeachersOnPage(teachers) {
    TEACHER_LIST.insertAdjacentHTML(
        "beforeend", 
        teachers);
}

function addFavoriteTeachersOnPage(teachers) {
    FAV_RIGHT_ARROW.insertAdjacentHTML(
        "beforebegin", 
        teachers
    );
}

function calcAge(bDay) {
    const today = new Date();
    const age = today.getFullYear() - bDay.getFullYear();
    const monthDiff = today.getMonth() - bDay.getMonth();
    if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < bDay.getDate())) {
        age--;
    }
    return age;
}

function closeAddTeacherWindow() {
    TEACHER_FORM.name.value = TEACHER_FORM.course.value = TEACHER_FORM.country.value = TEACHER_FORM.city.value = TEACHER_FORM.b_date.value = TEACHER_FORM.phone.value = TEACHER_FORM.email.value = TEACHER_FORM.color.value = '';
    TEACHER_FORM.genderMale.checked = TEACHER_FORM.genderFemale.checked = false;
    TEACHER_FORM.form.parentElement.parentElement.children[0].children[1].dispatchEvent(new Event("click"));
}

// Task 2.1
function parseTeachers(teachersRaw) {
    const teachersResult = [];
    let teacherTemp;
    _.forEach(teachersRaw, teacherRaw => {
        // Assign regular props first
        teacherTemp = {
            gender: teacherRaw.gender,
            title: teacherRaw.name.title,
            full_name: `${teacherRaw.name.first} ${teacherRaw.name.last}`,
            city: teacherRaw.location.city,
            state: teacherRaw.location.state,
            country: teacherRaw.location.country,
            postcode: teacherRaw.location.postcode,
            coordinates: teacherRaw.location.coordinates,
            timezone: teacherRaw.location.timezone,
            email: teacherRaw.email,
            b_date: teacherRaw.dob.date,
            age: teacherRaw.dob.age,
            phone: teacherRaw.phone,
            picture_large: teacherRaw.picture.large,
            picture_thumbnail: teacherRaw.picture.large
        };
        // Determine id
        if(!teacherRaw.id.name || !teacherRaw.id.value) {
            teacherTemp.id = `${++teacherGenId}`;
        } else {
            teacherTemp.id = `${teacherRaw.id.name}${teacherRaw.id.value}`;
        }
        // Add additional props with default values
        teacherTemp = getTeacherWithAdditionalProps(teacherTemp);
        // Add a teacher to the result only if it is valid
        if(isValid(teacherTemp)) {
            teachersResult.push(teacherTemp);
        }
    });
    return teachersResult;
}

function addNewTeachers(teachers, newTeachers) {
    const teachersResult = [...teachers];
    let duplicateIndex;
    let mergedTeacher;
    for(let newTeacher of newTeachers) {
        // Skip if a teacher already exists
        duplicateIndex = isDuplicate(teachers, newTeacher);
        if(duplicateIndex !== -1) {
            mergedTeacher = getMergedTeacher(teachers[duplicateIndex], newTeacher);
            if(isValid(mergedTeacher)) {
                teachersResult[duplicateIndex] = mergedTeacher;
            }
            continue;
        }
        // Add additional props with default values
        newTeacher = getTeacherWithAdditionalProps(newTeacher);
        // Add a teacher to the result only if it is valid
        if(isValid(newTeacher)) {
            teachersResult.unshift(newTeacher);
        } 
    }
    return teachersResult;
}

// Task 2.2 and 5.3
function isValid(teacher) {
    if(!isValidProp(teacher.country, "string", (prop) => prop.charAt(0) === prop.charAt(0).toUpperCase())) {
        console.log(`Attempt to add a teacher (${teacher.id}) with an invalid country (${teacher.country})`);
        return false;
    }
    const locale = COUNTRY_TO_SHORT[teacher.country];
    const props = [
        {
            propName: "full_name", 
            propType: "string",
            propCheck: (prop) => prop.charAt(0) === prop.charAt(0).toLocaleUpperCase(locale)
        }, 
        {
            propName: "note", 
            propType: "string",
            propCheck: (prop) => prop.charAt(0) === prop.charAt(0).toLocaleUpperCase(locale)
        }, 
        {
            propName: "state", 
            propType: "string",
            propCheck: (prop) => prop.charAt(0) === prop.charAt(0).toLocaleUpperCase(locale)
        }, 
        {
            propName: "city", 
            propType: "string",
            propCheck: (prop) => prop.charAt(0) === prop.charAt(0).toLocaleUpperCase(locale)
        },
        {
            propName: "gender", 
            propType: "string"
        },
        {
            propName: "age",
            propType: "number"
        },
        {
            propName: "phone",
            propType: "string",
            propCheck: (prop) => isValidPhoneNumber(prop, locale)
        },
        {
            propName: "email",
            propType: "string",
            propCheck: (prop) => EMAIL_REGEX.test(prop)
        }
    ];
    let res = true;
    _.every(props, prop => {
        if(!isValidProp(teacher[prop.propName], prop.propType, prop.propCheck)) {
            console.log(`Attempt to add a teacher (${teacher.id}) with an invalid ${prop.propName} (${teacher[prop.propName]})`);
            res = false;
            return false;
        }
        return true;
    });
    return res;

}

// Task 2.3
function getFilteredTeachers(teachers, reqRegion, reqAgeMin, reqAgeMax, reqGender, reqFavorite, reqPhoto) {
    const filteredTeachers = _.filter(teachers, teacher => 
            (!reqRegion || getRegion(teacher.country) === reqRegion) && 
            (!reqAgeMin || teacher.age >= reqAgeMin) && 
            (!reqAgeMax || teacher.age <= reqAgeMax) && 
            (!reqGender || teacher.gender === reqGender) && 
            (!reqFavorite || teacher.favorite === reqFavorite) &&
            (!reqPhoto || teacher.picture_large || teacher.picture_thumbnail)
        );
    return filteredTeachers;
}

// Task 2.4
function getSortedTeachers(teachers, sortBy, desc) {
    return teachers.toSorted(
        (teacher1, teacher2) => {
            // if (typeof teacher1[sortBy] === 'string') {
            //     return teacher1[sortBy].localeCompare(teacher2[sortBy], COUNTRY_TO_SHORT[teacher1.country]);
            // } else {
                let res = (teacher1[sortBy] > teacher2[sortBy]) - (teacher1[sortBy] < teacher2[sortBy]);
                res = desc ? -1 * res : res;
                return res;
            //}
        }
    );
}

// Task 2.5
function getQueryTeachers(teachers, query) {
    const queryTeachers = _.filter(teachers, teacher => 
            teacher["full_name"].includes(query) || 
            teacher["note"].includes(query) || 
            teacher["age"] == query);
    return queryTeachers;
}

// Task 2.6
function getQueryTeacherHit(allTeachers, queriedTeachers) {
    return 100 * queriedTeachers.length / allTeachers.length;
}

// Helper functions Lab 2

// index of a duplicate element
// -1 if not duplicate
// duplicate is determined either by an id or by an email
function isDuplicate(teachers, newTeacher) {
    for(let i = 0; i < teachers.length; i++) {
        if(teachers[i].id === newTeacher.id || teachers[i].email === newTeacher.email) {
            return i;
        }
    }
    return -1;
}

function isValidProp(prop, propType, additionalCheck) {
    return typeof prop === propType && (!additionalCheck || additionalCheck(prop));
}

function getMergedTeacher(teacher1, teacher2) {
    const mergedTeacher = Object.assign({}, teacher1);
    for(const prop in teacher2) {
        if (teacher2[prop]) {
            mergedTeacher[prop] = teacher2[prop];
        }
    }
    return mergedTeacher;
}

function getCourse() {
    const courseId = Math.floor(Math.random() * COURSES.length);
    return COURSES[courseId];
}

function getTeacherWithAdditionalProps(teacher) {
    const teacherResult = Object.assign({}, teacher);
    _.forEach(ADDITIONAL_PROPS, additionalProp => {
        if(!teacherResult[additionalProp.propName]) {
            teacherResult[additionalProp.propName] = eval(additionalProp.defaultValue);
        }
    });
    return teacherResult;
}

function getRegion(country) {
    for(const region in REGIONS_OF_COUNTRIES) {
        if(REGIONS_OF_COUNTRIES[region].includes(country)) {
            return region;
        }
    }
    return null;
}